% function [header, DATA] = readcsv(inputfile)
%
% Read CSV with text header
%
% Made by Ismael
%
% September 2019
% Bremerhaven

function [header, DATA] = readcsv(inputfile)
   rr1 = 2e2;
   rr2 = 1e4;
   % rr1 = 2e1;
   % rr2 = 1e3;

   fid = fopen(inputfile, 'r');

   ln = 1;

   header = fgetl(fid);

   header = strsplit(header, ",");

   cc = 1;

   while ln >0

      ln = fgetl(fid);

      if ln<0
         break
      end

      tl = strsplit(ln, ",");

      for k = 1:length(tl)
         datum = str2num(tl{k});
         if isempty(datum)
            datum = NaN;
         end
         if length(datum)>1  % tl{k} was a string, not a number...
             DATA(cc,k) = NaN; % At the moment we cannot do anything else, since we cannot mix numbers a strings in a matrix (contrary to the data frames in R)
         else
             DATA(cc,k) = datum;
         end
      end

      % Show something in screen just to know something is going on:
      if mod(cc, rr1) == 0
         fprintf(".")
      end
      if mod(cc, rr2) == 0
         % fprintf("x\n")
         fprintf("%d lines of code read...\n", cc)
      end

      cc = cc+1;

   end
   if cc>rr1
      fprintf("\n")
   end

   fclose(fid);
end % Function
