      subroutine asm_main(dat1,dat2,ndat,lag1,lag2,edof,critv)
c-----------------------------------
c       On input of a pair of time series dat1 and dat2 with ndat data
c       values, computes estimate of the 95% significance level of
c       their correlation based on the long-lag artificial skill method.
c       User can specify the range of lags from lag1 to lag2 over which
c       the artificial skill is estimated. If lag1 and lag2 are
c       specified as 0, the subroutine defaults to values of
c       lag1=0.4*ndat and lag2=0.8*ndat.
c
c  By Dudley Chelton with modifications by Ismael Nunez-Riboni
c
c January, 2022
c April, 2022
c-----------------------------------

      dimension dat1(ndat),dat2(ndat)
      parameter (ntmax=3960) ! length(tp2) in program make_tinv95.m

      REAL :: Ndfminus2, tp2

      npair=0
      do 110 i=1,ndat
      if(dat1(i).eq.amiss.or.dat2(i).eq.amiss) go to 110
      npair=npair+1
  110 continue

      ! Defining default initial and final lags k1 and k2. Excluding 40% of the lags near
      ! lag zero avoids real skill in the computation (note the difference to
      ! the MC simulations in Nunez-Riboni et al 2022 where all lags were included;
      ! see in particular Section S4 in the supplement):
      if(lag1.eq.0) lag1=.4*ndat
      if(lag2.eq.0) lag2=.8*ndat

      edof=asm_edof_art(dat1,dat2,ndat,lag1,lag2)

      ! print *, 'edof=',edof

      ! ! Approximation:
      ! chi05=3.84 ! This is only valid for the 95% significance
      ! critv=sqrt(chi05/edof) ! Simplified equation

      ! To use the exact relation, we need a t score with Ndf-2 degrees of freedom:
      open(15,file='tinv95.csv',status='old') ! 'tinv95.csv' created with make_tinv95.m
      read (15,*) ! to skip first line, with header
      do i=1,ntmax
        read(15,*) Ndfminus2,tp2

        if (Ndfminus2 >= (edof-2)) then ! Any (N*-2) exceeding 200 receives the score 1.9720
          exit
        end if

      enddo
      close(15)
      ! Exact relation:
      critv =  sqrt(tp2**2 / (tp2**2 + edof - 2)) ! This is the same as Eq. 2 of P&P98, corrected with the erratum

      return
      end
