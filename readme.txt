Description:

The cross-correlation coefficient between time series is a common tool to study and quantify the degree
of relation between two variables. The traditional method for statistical significance of correlation
(included in common programming languages like Octave and R) relies on the assumption that the data are
independent. However, many time series are strongly auto-correlated. Such auto-correlation reduces the
effective number of independent realizations and leads to an erroneous conclusion of statistically
significant correlation where no real relation exists. The “artificial skill method” (ASM; Chelton,
1983) for correlation significance addresses this issue and yields a more accurate estimation
of significance also when the data are serially auto-correlated.

This repository contains the ASM code as tuned in the (open access!) article:

Núñez-Riboni I, Chelton, D and Marconi V, 2023. The spectral color of natural and anthropogenic
time series and its impact on the statistical significance of cross correlation.
Science of the Total Environment, Volume 860, 160219, ISSN 0048-9697,
https://doi.org/10.1016/j.scitotenv.2022.160219

The code is available in Octave/MATLAB (asm_main.m), R (asm_main.R) and FORTRAN (asm_main.f). There
is also one example program for each programming language, showing how to use the functions (e.g.,
asm_example.R). The most comprehensive example is the one in Octave (asm_example.m), which also
makes some plots. Please take a look at the comments there and at the graphics which it
generates (they are in the repository).

****************************
Please note the following! :
****************************

1) Carefully read the complete documentation inside each of the corresponding functions
before using them!

2) If you use this code, please quote our paper!

3) Use at your own risk!

Questions, suggestions, wishes and comments can be sent to:
Ismael Nunez-Riboni
E-Mail: ismael.nunez-riboni@thuenen.de
WWW: https://www.thuenen.de/de/fachinstitute/seefischerei/personal/wissenschaftliches-personal/nunez-riboni-ismael-dr

Ismael Nunez-Riboni, January of 2022
(last change: 7th of June, 2023)
