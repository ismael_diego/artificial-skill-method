      subroutine asm_xcorr_baj(dat1,dat2,n,lag,cor,ndat)
c-----------------------------------------------------------
c       Computes correlation between time series dat1 at time
c       t and time series dat2 at time (t+lag), i.e., positive
c       lag indicates time series dat2 follows dat1 by amount
c       lag.  N is number of observations in both time series.
c       All statistics are computed over the same set of
c       observations.  Missing data are flagged as 1.e35 on
c       input.  Note that, even if there are no missing
c       observations, statistics are computed over fewer than
c       N observations for nonzero lag.  Number of data over
c       which statistics are computed is returned in ndat.
c       To reduce roundoff error, it makes 2 passes through the data.
c
c  By Dudley Chelton with modifications by Ismael Nunez-Riboni
c
C-----------------------------------------------------------
      parameter (amiss=1.e35)
      dimension dat1(n),dat2(n)

      ! MEANS !

      ave1=0.
      ave2=0.
      ndat=0

      ! Calculation of means should use all N lags, as given by the
      ! Box and Jenkins estimator for correlation:

      do 300 i=1,n
      dd1=dat1(i)
      dd2=dat2(i)
      if(dd1.eq.amiss.or.dd2.eq.amiss) go to 300
      ndat=ndat+1
      ave1=ave1+dd1
      ave2=ave2+dd2
  300 continue
      ave1=ave1/ndat
      ave2=ave2/ndat

      ! VARIANCES !

      var1=0.
      var2=0.
      ndat=0

      ! Calculation of variances should also use all N lags, as given by the
      ! Box and Jenkins estimator for correlation:

      do 400 i=1,n
      dd1=dat1(i)
      dd2=dat2(i)
      if(dd1.eq.amiss.or.dd2.eq.amiss) go to 400
      ndat=ndat+1
      var1=var1+(dd1-ave1)**2
      var2=var2+(dd2-ave2)**2
  400 continue

      var1=var1/ndat !
      var2=var2/ndat

      ! COVARIANCE !

      cov=0.
      ndat=0

      ! Calculation of covariance uses only (N-j) lags:

      if(lag.lt.0) then ! Lag is negative
        i1=-lag+1       ! If lag is -3, i1 = 4, starting the run with 4th element
        i2=n            ! and finishing with last element (removing 3 elements from the begining)
      else
        i1=1
        i2=n-lag
      endif

      do 500 i=i1,i2
      dd1=dat1(i)
      dd2=dat2(i+lag)
      if(dd1.eq.amiss.or.dd2.eq.amiss) go to 500
      ndat=ndat+1
      cov=cov+(dd1-ave1)*(dd2-ave2)
  500 continue

      cov=cov/ndat
      cor=cov/sqrt(var1*var2)

      ! print *, "lag, cov, var1, var2", lag, cov, var1, var2

      return
      end
