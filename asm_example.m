% asm_example.m
%
% Test for the octave function for the Artificial Skill Method for significance
% of correlation.
%
% By Ismael Nunez-Riboni
% Thuenen Institute of Sea Fisheries
% Bremerhaven
% January, 2022; December 2024
%
% Output should be:
%
% Correlation coefficient rho = -0.383499
% Record length N = 833.000000
% Effective number of degrees of freedom N* = 73.406432  
% Critical value for correlation = 0.229635
% p value = 0.000783
%
% Because abs(rho) = 0.38350 is larger than the critical value (=0.22963),
% the correlation is significant (with more than 95% probability).
%
% See further notes at the bottom of this file!

% Prescribed significance (95%):
alpha = 0.05;

% Use your own path to this file here:
inputfile = "/home/ismael/BS/programs/asm/alaska_smooth_17p.txt";
% inputfile = "/home/ismael/BS_backup/programs/asm/check_2412/Delaware_SSTA.csv";

[header, DATA] = readcsv(inputfile);

% Select your own columns of data (here they are the 2nd and 3rd):
indxA=DATA(:,2); % For Alaska time series
indxB=DATA(:,3);
% indxA=DATA(:,1); % For Delaware time series
% indxB=DATA(:,2);

N = length(indxA); % record length

C = corr(indxA, indxB);  % Correlation coefficient

% Estimating effective number of degrees of freedom Ndf, p-value pval and
% critical value critv:
[Ndf, pval, critv] = asm_main(indxA, indxB, alpha);

% Output:

fprintf("Correlation coefficient rho = %f\n",C)
fprintf("Record length N = %f\n",N)
fprintf("Effective number of degrees of freedom N* = %f\n",Ndf)
fprintf("Critical value for correlation = %f\n",critv)
fprintf("p value = %f\n",pval)

% Some plots

lw = 4; % Line width

% Let us take a look to the time series;
% To better apreciate the relation between the two time series,
% it is good to scale them by their STD and multiply one of them
% by -1:
figure
hold on
plot(indxA./std(indxA),"linewidth",lw)  % Scaling through the STD is only...
plot(-indxB./std(indxB),"linewidth",lw)  % ...to plot both time series together
xlabel("Time")

% And now let us take a look to their cross correlation.
% Note here the following:
% 1) For this plot, the Octave's built-in function xcorr is good enough
% but inside the ASM function we use the unbiased estimator of Box and Jenkins
% (there are reasons for that, please see the paper). Therefore, for consistency,
% we also use here that same estimator;
% 2) A similar plot can help you find which is the lag of maximum correlation
% between your time series. If this lag is *not* zero, you should displace
% one of the time series until the maximum correlation is at lag zero. Otherwise,
% the ASM significance test will not work as it should.

[Cor, LAG, NN] = asm_xcorr_baj(indxA, -indxB); % The scaling with STD for the plot is here irrelevant

figure
plot(LAG, Cor,"linewidth",lw)
hold on
grid on
ylabel("Corr")
xlabel("Lag")

% Finally, let us mark the 40 and 80% lags. All lags inbetween are used in
% the ASM averages. Note that, in this example, the correlation function
% quickly decays to 0. Therefore, lags shorter than 40% could be included in the
% computation as well. However, as described in Section S4 of our paper, k1=40%
% should normally be a safe value;

k1= round(max(LAG).*0.4);
k2= round(max(LAG).*0.8);

plot([k1,k1],[-1,1],"k-","linewidth",lw*2)
plot([k2,k2],[-1,1],"k-","linewidth",lw*2)
plot(-[k1,k1],[-1,1],"k-","linewidth",lw*2)
plot(-[k2,k2],[-1,1],"k-","linewidth",lw*2)

offsetX = 0.05;
offsetY = 1.05;
text(k1+offsetX, offsetY,"0.4N")
text(k2+offsetX, offsetY,"0.8N")
text(-k1-offsetX, offsetY,"-0.4N")
text(-k2-offsetX, offsetY,"-0.8N")

text(50, 1.4,["Record length N = ",num2str(N)])

set(gca, "ylim",[-1,1].*1.5)

% Note the output plots of this program are saved in the git repository:
% output_of_asm_example_octave1.png
% output_of_asm_example_octave2.png

% Notes
%
% (1) Dec 13th, 2024: 
% The N* reported here is obtained by using the Box and Jenkins estimator
% for cross correlation. If you use the "traditional" estimator for correlation used
% in previous analyses with ASM, you should obtain the value N* = 77.631923.
%
% Similarly, for the Delaware time series, N* = 82.136926 with the present code,
% but it is N* = 63.215374 by using the traditional ASM estimator for correlation.
%
% Please note that the traditional ASM estimator for correlation has been shown in NR et al 2023
% to be slightly less accurate than the Box and Jenkins estimator. The difference
% between the two estimators is that the traditional estimator computes the means 
% and variances of X(t) and Y(t) in our Equation S4 by using the same (N-k) 
% elements that are included in the cross covariance at lag k that is in the numerator. 
% The Box and Jenkins estimator computes the means and variances of X(t) and Y(t) 
% by summing all N elements (see Supplement of our publication, Equation S4 and the 
% text immediately below).
%
% If you have good reasons to use the traditional ASM estimator (e.g., for comparisons), 
% you can call inside the function asm_main.m the function "xcorrel" (included now in the 
% git repository) instead of the function "asm_xcorr_baj".