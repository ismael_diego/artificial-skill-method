function [Ndf, pval, critv] = asm_main(varargin);
% function [Ndf, pval, critv] = asm_main(ts1, ts2, alpha,k1,k2);
%
% Effective number of degrees of freedom Ndf and p value pval for Pearson
% correlation between time series ts1 and ts2 accounting for auto-correlation
% following the "Artificial Skin Method" (ASM) from Chelton 1983. The p value is
% the probability that the correlation is not significantly different than zero.
% A bit redundantly, the function also gives the critical value critv over which
% the correlation is significant for a given prescribed probability alpha. Parameter
% alpha is optional and, if not specified, taken as alpha = 0.05 which gives the
% 95% significance level.
%
% ts1 and ts2 are time series with the same number of observations N.
% Ndf, pval and critv are scalars corresponding to correlation at lag 0. Optional
% parameters k1 and k2 specify the lags (as percentage 0-100 of the total record
% length) of the cross-correlation function involved in the computation. If not
% specified, they take default values k1 = 40% and k2 = 80%. Please
% see the reference below for further details.
%
% This function assumes that the maximum correlation occurs at lag zero, if this
% is not your case, you should shift one of your time series as to obtain a
% maximum of the cross-correlation function at lag zero.
%
% This function has been written and tested under Octave, but it should
% run under Matlab as well.
%
% Ismael Nunez-Riboni and Dudley Chelton with kind support from Craig Risien.
% V1: May, 2020; V2: January, 2022; V3: April, 2022; V4: December, 2024
%
% Thuenen Institute of Sea Fisheries
% Bremerhaven, Germany.
%
% Reference:
%
% Núñez-Riboni I, Chelton, D and Marconi V, 2022. The spectral color of
% natural and anthropogenic time series and its impact on the statistical
% significance of cross correlation. Science of the Total Environment (under review
% July 2022).

pkg load statistics

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Assigning values to input parameters %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if length(varargin) < 2
    fprintf('Not enough input arguments!\n')
    return
end

ts1 = varargin{1}(:);
ts2 = varargin{2}(:);

nn = length(ts1); % Record length

if nn ~= length(ts2)
    fprintf('ts1 and ts2 do not have the same number of elements!\n')
    return
end

if length(varargin) == 2 % No alpha, k1 or k2 specified
   alpha = 0.05; % Default value implies 95% significance level
   k1    = 40; % Default value
   k2    = 80; % Default value
   % Excluding 40% of the lags near lag zero to avoid real skill in the computation;
   % Note the difference to the MC simulations in Nunez-Riboni et al 2022 where
   % all lags were included (see in particular Section S4 in the supplement)
end

if length(varargin) == 3  % Parameter alpha is given by user (but k1 and k2 are not)
   alpha = varargin{3};
   k1    = 40; % Default value
   k2    = 80; % Default value
end

if length(varargin) == 4  % Parameters alpha and k1 are given by user (but k2 is not)
   alpha = varargin{3};
   k1    = varargin{4};
   k2    = 80; % Default value
end

if length(varargin) == 5  % All parameters are given by user
   alpha = varargin{3};
   k1    = varargin{4};
   k2    = varargin{5};
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Unbiased estimator for correlation %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[Cor, LAG, NN] = asm_xcorr_baj(ts2, ts1); % Box and Jenkins estimator for correlation; ts1 leads. Do not replace the call to this function with the built-in function from Octave/Matlab!
%[Cor, LAG, NN] = xcorrel(ts2, ts1); % Only for exact comparison with Fortran's code of D. Chelton;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculating effective number of degrees of freedom %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Remember k1 and k2 are given as %, (for instance 30%)
% and not as fraction 0-1.
k1 = floor(k1*nn/100);       % Initial lag
k2 = floor(k2*nn/100);       % Final lag

% The number of lags to include in the computation (if no missing data) si K = k2 - k1 + 1
% but we do not need this variable in this code (since we calculate means with built-in
% functions);

if any(isnan(ts1)) | any(isnan(ts2))  % Missing data
   nn = sum(~isnan(ts1) & ~isnan(ts2));  %
end

% Eliminate the "borders" of the correlation function:
badies = LAG < -k2 | LAG >  k2;
if sum(badies) <=2 % k is probably too large for short time series and, in this case, we eliminate only the first and last lags.
   badies(1:2) = 1;    % Therefore, we demand to remove at least 2 lags from begining...
   badies(end-1:end) = 1; %...and end
end
NN(badies)  = [];
Cor(badies) = [];
LAG(badies) = [];

% We are assuming that the maximum correlation occurs at lag zero:
lagmax = 0;
% Alternatively to shifting one time series, you can obtain the lag of
% maximum correlation with:
% lagmax = LAG(abs(Cor) == max(abs(Cor)));

if length(lagmax) > 1 % If you use lagmax = LAG(abs(Cor) == max(abs(Cor))):
  keyboard % It can be that the correlation function has more than 1 maximum (it happens with short time series and if k2 is large)
%   The solution should be to reduce k2;
end

% We now eliminate k1% of the lagged correlations before and after the maximum:
badies = LAG > lagmax-k1 & LAG <  lagmax + k1;

% Eliminate these lags:
nnlags = NN(~badies);   %
lags   = LAG(~badies);   % Relevant for the plot only
ccf    = Cor(~badies);   %

% Now we calculate A = S*Nk as usual (using the remaining lags):
%A = nanmean( nnlags .* ccf.^2 );  % This is the same as A = 1/length(nnlags) * nansum( nnlags .* ccf.^2 );  %
A = mean( nnlags .* ccf.^2 , "omitnan");  % This is the same as A = 1/length(nnlags) * nansum( nnlags .* ccf.^2 );  %

% Equation S2:
nu = 1/A;

% Equation S1b:
Ndf = nu.*nn;  % Effective number of degrees of freedom;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Calculating critical value %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% The significance level is constructed with alpha/2 (and not with alpha) because
% the t distribution is two-sided:

% Eq. S5:
tp2 = tinv(1-alpha/2, Ndf - 2); % t score at alpha with Ndf - 2 degrees of freedom
critv =  sqrt(tp2.^2 ./ (tp2.^2 + Ndf - 2)); % This is the same as Eq. 2 of P&P, corrected with the erratum

%%%%%%%%%%%%%%%%%%%%%%%
% Calculating p value %
%%%%%%%%%%%%%%%%%%%%%%%

% This is, in a way, redundant with the previous analysis, since we can already
% know if the correlation is significant or not under alpha. However, some
% people also want to know what is the the p-value and, with it, check
% with which confidence is the obtained correlation significant.

% We assume that the critical value is as large as the observed correlation
% and check how large must be p:
cv = Cor(LAG==0);

% Inverse of the critical value critv for Pearson derived using the t distribution, i.e.,
% inverse of cv =  sqrt(tp2.^2 ./ (tp2.^2 + Ndf - 2)) :
if Ndf>2
   t_alpha = sqrt(cv^2 * (Ndf - 2) / (1 - cv^2));
else
   t_alpha = NA;
end

% We now clear p (alpha) to the l.h.s of the equation of critical value, where now we need
% the cumulative t distribution function. I.e., this is basically the inverse of
% tp2 = tinv(1-alpha/2, Ndf - 2):
pval = 2*(1-tcdf(t_alpha, Ndf-2)); % P-value adjusted for auto-correlation.
