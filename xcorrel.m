% function [Cor, LAG, NN] = xcorrel.m(ts2, ts1);
% Unbiased estimator for correlation based on correl.f from Dudley Chelton
%
% 2nd argument ts1 leads when lag>0
%
% Note this function is *not* the same as
% [Cor, LAG]  = xcorr(ts2, ts1); 
% and not even the same as:
% [Cor, LAG]  = xcorr(ts2, ts1,'unbiased');
%
% This is very similar (though not exatly the same as):
% [Cor_, LAG_]  = xcorr(ts2, ts1,'coeff');
% For positive lags they differ on the order of 0.01. 
% The reason seems to be the way xcorr works, which is based on the FFT.
%
% This function gives identical results to Dudleys function correl.f.
% It is an unbiased estimator of correlation and includes implicitly 
% a normalization similar to the one proposed by Chadfield?
%
% Ismael Nunez-Riboni
% Thuenen Institute of Sea Fisheries
% BHV June 2020

function [Cor, LAG, NN] = xcorrel(ts2, ts1);

   ts1 = ts1(:);
   ts2 = ts2(:);

   nn = length(ts1); % Record length

   if nn ~= length(ts2)
       fprintf('ts1 and ts2 do not have the same number of elements!\n')
       return
   end

   for l = [(-nn + 1) : (nn -1) ] % Lags (-nn+1) & (-nn+2) ( the same that (nn-2) & (nn-1)) make no sense, since they correspond to time series with only 1 (result NA) and 2 (result 1) obs. But like this, the dimensions match.
      if(l<0)
        i1=-l+1; % Example: If lag is -3, i1 = 4, starting the run with 4th element
        i2=nn;   % and finishing with last element (i.e., removing 3 elements from the begining)
      else
        i1=1;
        i2=nn-l;
      end
      
      d1 = ts1(i1:i2);
      d2 = ts2((i1+l):(i2+l));
      
      badies = isnan(d1) | isnan(d2);
      d1(badies) = [];
      d2(badies) = [];

      LAG(l+nn) = l;
      NN(l+nn) = sum(~badies); % Number of data involved in each lagged correlation

      % This is in agreement with Dudley's correl.f and must be
      % used for the ASM method:

%      covAB = (d1 - nanmean(d1)) .* (d2 - nanmean(d2)); 
%      varA  = (d1 - nanmean(d1)).^2;  % 
%      varB  = (d2 - nanmean(d2)).^2;  % 
      
%      if (nanmean(varA) == 0) || (nanmean(varB) == 0)
%         Cor(l+nn) = NaN;
%      else
%         Cor(l+nn) = nanmean(covAB)/sqrt( nanmean(varA)*nanmean(varB) );  % For correl.f estimator (by correct choice of covAB, etc). Also P&P with Chadfield, Eq. 7 of P&P, also for missing values
%      end

      covAB = (d1 - mean(d1, "omitnan")) .* (d2 - mean(d2, "omitnan")); 
      varA  = (d1 - mean(d1, "omitnan")).^2;  % 
      varB  = (d2 - mean(d2, "omitnan")).^2;  % 
      
      if (mean(varA, "omitnan") == 0) || (mean(varB, "omitnan") == 0)
         Cor(l+nn) = NaN;
      else
         Cor(l+nn) = mean(covAB, "omitnan")/sqrt( mean(varA, "omitnan")*mean(varB, "omitnan") );  % For correl.f estimator (by correct choice of covAB, etc). Also P&P with Chadfield, Eq. 7 of P&P, also for missing values
      end

   end % for l
end % function