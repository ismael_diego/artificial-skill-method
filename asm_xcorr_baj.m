% function [Cor, LAG, NN] = asm_xcorr_baj(ts2, ts1);
%
% Unbiased estimator for correlation based on Box and Jenkins
% including Chadfield normalization.
%
% 2nd argument ts1 leads when lag>0
%
% Ismael Nunez-Riboni
% Thuenen Institute of Sea Fisheries
% BHV June 2020

function [Cor, LAG, NN] = asm_xcorr_baj(ts2, ts1);

   ts1 = ts1(:);
   ts2 = ts2(:);

   nn = length(ts1); % Record length

   if nn ~= length(ts2)
       fprintf('ts1 and ts2 do not have the same number of elements!\n')
       return
   end

   for l = [(-nn + 1) : (nn -1) ] % Lags (-nn+1) & (-nn+2) ( the same that (nn-2) & (nn-1)) make no sense, since they correspond to time series with only 1 (result NA) and 2 (result 1) obs. But like this, the dimensions match.
      if(l<0)
        i1=-l+1; % Example: If lag is -3, i1 = 4, starting the run with 4th element
        i2=nn;   % and finishing with last element (i.e., removing 3 elements from the begining)
      else
        i1=1;
        i2=nn-l;
      end

      d1 = ts1(i1:i2);
      d2 = ts2((i1+l):(i2+l));

      badies = isnan(d1) | isnan(d2);
      d1(badies) = [];
      d2(badies) = [];

      LAG(l+nn) = l;
      NN(l+nn) = sum(~badies); % Number of data involved in each lagged correlation
      % if NN(l+nn) is zero is because there are no valid data in ts1 and ts2 (one has only NAs)

      % See P&P's Eq. 6:
%      covAB = (d1 - nanmean(ts1)) .* (d2 - nanmean(ts2)); %
%      varA  = (ts1 - nanmean(ts1)).^2;  %
%      varB  = (ts2 - nanmean(ts2)).^2;  %

      covAB = (d1 - mean(ts1, "omitnan")) .* (d2 - mean(ts2, "omitnan")); %
      varA  = (ts1 - mean(ts1, "omitnan")).^2;  %
      varB  = (ts2 - mean(ts2, "omitnan")).^2;  %


%      if (nanmean(varA) == 0) || (nanmean(varB) == 0)
      if (mean(varA, "omitnan") == 0) || (mean(varB, "omitnan") == 0)
         Cor(l+nn) = NaN;
      else
         % Box and Jenkins estimator with Chadfield normalization
         % (see P&P's Eq6 and 7):
%         Cor(l+nn) = nanmean(covAB)/sqrt( nanmean(varA)*nanmean(varB) );  % P&P with Chadfield, Eq. 7 of P&P, also for missing values
         Cor(l+nn) = mean(covAB, "omitnan")/sqrt( mean(varA, "omitnan")*mean(varB, "omitnan") );  % P&P with Chadfield, Eq. 7 of P&P, also for missing values

%         Cor(l+nn) = nansum(covAB)/sqrt( nansum(varA)*nansum(varB) );  % P&P *without* Chadfield eq. 6 of P&P, when no data is missing

      end

   end % for l

end % function
