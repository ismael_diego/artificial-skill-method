% make_tinv95.m
%
% This programs generates a CSV table file with t scores (95% confidence) for
% varios values of effective number of degrees of freedom N*.
% The goal of this text table is to be read with the Fortran
% code that calculates the significance of the cross correlation using the
% ASM method (and because I did not find in Internet a similar Fortran
% function calculating t scores).
%
% Ismael Nunez-Riboni
% Thünen Institute of Sea Fisheries
% Bremerhaven, Germany
% April, 2022

alpha = 0.05; % Prescribed confidence for the t score table

dN = 0.05; % Table's resolution
maxNdf = 200; % Maximum computed value

Ndf = [2 + dN : dN : maxNdf];

tp2 = tinv(1-alpha/2, Ndf - 2);

figure
plot(Ndf, tp2)

ofilecsv = "/home/ismael/BS/programs/asm/tinv95.csv"

writecsv(ofilecsv, [Ndf(:), tp2(:)], "Ndf-2,tscore")

% Check:

critv =  sqrt(tp2.^2 ./ (tp2.^2 + Ndf - 2)); % This is the same as Eq. 2 of P&P, corrected with the erratum
figure
plot(Ndf, critv,"k","linewidth",6)
grid on
xlabel("Effective Degrees of Freedom, N*")
ylabel("Critical value")
set(gca,"xlim",[0,160])
