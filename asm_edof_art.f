      function asm_edof_art(data1,data2,nobs,lag1,lag2)
c-------------------
c       Computes estimate of effective degrees of freedom N* by the
c       long-lag artificial skill method.
c       The "artificial skill values" are averaged over long lags
c       between (-lag2 to -lag1) and (lag1 to lag2).
c
c       Missing data are flagged as 1.e35.
c
c  By Dudley Chelton with some modifications by Ismael Nunez-Riboni
c
c-------------------
      parameter (amiss=1.e35)
      dimension data1(nobs),data2(nobs)

      ! REAL :: edof_art

      nsum=0
      sumSN=0.

      do 500 ipass=1,2
      if(ipass.eq.1) then
        llag1=-lag2
        llag2=-lag1
      else
        llag1=lag1
        llag2=lag2
      endif
      do 200 lag=llag1,llag2

      call asm_xcorr_baj(data1,data2,nobs,lag,cor,ncor)

      cor2=cor*cor
      SN=cor2*ncor

      nsum=nsum+1
      sumSN=sumSN+SN

  200 continue
  500 continue

c     sumSN is A (eq. 9.6.6); nsum is 2*K because of the do 500 which has 2 iterations (one over negative, one over positive lags)
      sumSN=sumSN/nsum

      factor=1./sumSN

  !     print 556, factor
  ! 556 format(1x'   factor =          ',f10.8)

c       determine number of paired sample obs at zero lag
c      this is the total number of observations N
      npair=0
      do 600 i=1,nobs
      if(data1(i).ne.amiss.and.data2(i).ne.amiss) npair=npair+1
  600 continue

      asm_edof_art=factor*npair

      return
      end
