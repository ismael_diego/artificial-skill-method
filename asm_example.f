c  asm_example.f
c
c  Test program for computing 95% significance levels
c  of correlations based on the artificial skill method.
c
c  The program computes the 95% significance levels for the correlations
c  between precipitaiton (pcp) and 500 hPa geopotential height (hgt) for
c  southeast Alaska.
c
c  By Dudley Chelton with modifications by Ismael Nunez-Riboni
c
C---------------------------------------------------------------
c
C Example of how to compile and execute under Linux:
c cd YOURDIR
c gfortran -fbounds-check -o asm_example asm_xcorr_baj.f asm_edof_art.f asm_main.f asm_example.f
c ./asm_example
c
c Expected output is:
c
! Input data file:alaska_smooth_17p.txt
!  lag1 =           333
!  lag2 =           666
!    Data number =           833
!    Effective degrees of freedom =           73.40651703
!    95% significance level for correlation =  0.2297
!    Sample correlation at zero lag =         -0.38349888

      parameter (ntmax=834,nxmax=225,amiss=1.e35)
      dimension pcp1(ntmax),hgt1(ntmax)
      ! character*17 filein
      ! character*20 filein
      character*21 filein
      ndat=833

      ! filein='PCP_500HGT_AK.txt'
      ! filein='alaska_smooth_3p.txt'
      filein='alaska_smooth_17p.txt'

      print *, 'Input data file:',filein

      open(15,file=filein,status='old',form='formatted')
      read (15,*) ! to skip first line, with header
      do i=1,ndat
        ! read(15,*) idat,iyr,mo,pcpi,hgti
        read(15,*) idat,pcpi,hgti
        if(pcpi.eq.-99) pcpi=amiss
        if(hgti.eq.-99) hgti=amiss
          pcp1(i)=pcpi
          hgt1(i)=hgti
      enddo
      close(15)

c       Calculate correlations and 95% significance levels for the time series
      lag=0
      lag1=.4*ndat  ! k1 = 40%
      lag2=.8*ndat  ! k2 = 80%

      print 35, lag1
   35 format(1x' lag1 =          ',i4)
      print 36, lag2
   36 format(1x' lag2 =          ',i4)

      call asm_xcorr_baj(pcp1,hgt1,ndat,lag,cor,nonmiss) ! Calculate the correlation

      call asm_main(pcp1,hgt1,ndat,lag1,lag2,edofart,c95art) ! Main fortran function for ASM

      print 37, ndat
   37 format(1x'   Data number =          ',i4)
      print 38, edofart
   38 format(1x'   Effective degrees of freedom =          ',f12.8)
      print 39, c95art
   39 format(1x'   95% significance level for correlation =',f8.4)
      print 40, cor
   40 format(1x,'   Sample correlation at zero lag =        ',f12.8)

      stop
      end
